variable "appId" {
  description = "Azure Kubernetes Service Cluster service principal"
}

variable "password" {
  description = "Azure Kubernetes Service Cluster password"
}

variable "aks_name" {
  type    = string
  default = "n3yron"
}

variable "admin_username" {
  default = "linux_admin"
}

variable "rg" {
  type    = string
  default = "n3yron"
}
#variable "backend_key" {
#  type = string
#}
