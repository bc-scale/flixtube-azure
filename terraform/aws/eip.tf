resource "aws_eip" "nat1" {
  depends_on = [aws_internet_gateway.gw]
  count      = var.deploy_private_subnets ? 1 : 0
}
resource "aws_eip" "nat2" {
  depends_on = [aws_internet_gateway.gw]
  count      = var.deploy_private_subnets ? 1 : 0
}
