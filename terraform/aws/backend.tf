terraform {
  backend "s3" {
    bucket         = "terraform-remote-state-aws-n3yron"
    key            = "dev/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-remote-state-aws-n3yron-table"
  }
}
